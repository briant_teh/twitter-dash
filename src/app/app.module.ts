import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewTweetsComponent } from './view-tweets/view-tweets.component';
import { HomeComponent } from './home/home.component';
import { TwitterApiService } from './services/twitter-api.service';

@NgModule({
    declarations: [
        AppComponent,
        ViewTweetsComponent,
        HomeComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [
        TwitterApiService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
