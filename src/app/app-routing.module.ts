import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ViewTweetsComponent } from './view-tweets/view-tweets.component';

const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'view-tweets', component: ViewTweetsComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
