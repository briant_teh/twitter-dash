import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TwitterApiService } from '../services/twitter-api.service';

declare var $: any;
declare var twttr: any;

@Component({
    selector: 'app-view-tweets',
    templateUrl: './view-tweets.component.html',
    styleUrls: ['./view-tweets.component.css']
})
export class ViewTweetsComponent implements OnInit {

    oauthToken: string;
    oauthVerifier: string;
    accessToken: string;
    accessTokenSecret: string;

    tweets: Array<any>;
    searchStr: string;

    constructor(
        private twitterApi: TwitterApiService,
        private route: ActivatedRoute,
        private zone: NgZone
    ) {
    }

    ngOnInit() {
        this.setAccessToken();

        if (!this.accessToken) {
            this.getOauthToken();
        } else {
            if (window.location.search.length > 0) {
                window.location.search = '';
            }
        }

        document.addEventListener('treadmillscrolled', (evt: any) => {
            const tweetId = evt.detail.id;

            const tweet = document.getElementById(tweetId);

            tweet.removeChild(tweet.childNodes[0]);
            
            twttr.widgets.createTweet(
                tweetId,
                document.getElementById(tweetId)
            )
        });
    }

    searchTweets(params): void {
        params.access_token = window.localStorage.getItem('access_token');
        params.access_token_secret = window.localStorage.getItem('access_token_secret');
        params.tweet_mode = 'extended';

        this.twitterApi.searchTweets(params)
            .subscribe(result => {
                if (result) {
                    this.tweets = result.statuses;
                    console.log('this.tweets', this.tweets);

                    if (this.tweets && this.tweets.length > 0) {
                        setTimeout(() => {
                            for (let t of this.tweets) {
                                twttr.widgets.createTweet(
                                    t.id_str,
                                    document.getElementById(t.id_str)
                                )
                                .then(result => {
                                    console.log('Tweet displayed...');
                                });
                            }
    
                            setTimeout(() => {
                                $(document).ready(() => {
                                    $('#tweet-treadmill').startTreadmill({
                                        runAfterPageLoad: true,
                                        direction: 'down',
                                        speed: 5000,
                                        viewable: 3,
                                        pause: false
                                    });
                                });
                            }, 1500);
                        }, 0)
                    }
                }
            });
    }

    loginTwitter(): void {
        console.log('Logging in to Twitter...');

        this.twitterApi.oauthRequestToken({})
            .subscribe(result => {
                if (result) {
                    console.log('result', result);
                    const resultJson: any = this.urlParamToJson(result);

                    window.localStorage.setItem('request_token', resultJson.oauth_token);
                    window.localStorage.setItem('request_token_secret', resultJson.oauth_token_secret);
                    window.location.href = 'https://api.twitter.com/oauth/authenticate?' + result;
                }
            });
    }

    getOembed(url: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.twitterApi.statusesOembed(url)
                .subscribe(result => {
                    if (result) {
                        resolve(result);
                    } else {
                        reject();
                    }
                })
        });
    }

    logoutTwitter(): void {
        window.localStorage.clear();
        window.location.reload();
    }

    getOauthToken(): void {
        this.route.queryParams.subscribe(params => {
            this.oauthToken = params['oauth_token'];
            this.oauthVerifier = params['oauth_verifier'];

            if (this.oauthToken) {
                if (this.tokenMatchesRequestToken(this.oauthToken)) {
                    this.twitterApi.oauthAccessToken({
                        request_token: window.localStorage.getItem('request_token'),
                        request_token_secret: window.localStorage.getItem('request_token_secret'),
                        oauth_verifier: this.oauthVerifier
                    })
                        .subscribe(result => {
                            if (result) {
                                console.log('result', result);

                                if (JSON.stringify(result).indexOf('oauth_token') > -1) {
                                    const resultJson = this.urlParamToJson(result);

                                    window.localStorage.setItem('access_token', resultJson.oauth_token);
                                    window.localStorage.setItem('access_token_secret', resultJson.oauth_token_secret);

                                    this.setAccessToken();
                                }
                            }
                        });
                }
            }
        });
    }

    setAccessToken(): void {
        this.accessToken = window.localStorage.getItem('access_token');
        this.accessTokenSecret = window.localStorage.getItem('access_token_secret');
    }

    buildOembedUrlFromTweet(tweet) {
        let url;

        if (tweet.entities.urls.length > 0) {
            url = tweet.entities.urls[0].expanded_url;
        } else {
            url = tweet.entities.media[0].expanded_url;
        }

        url = url.replace('i/web', tweet.user.screen_name);
        url = url.replace('/photo/1', '');
        return url;
    }

    tokenMatchesRequestToken(token): boolean {
        if (token === window.localStorage.getItem('request_token')) {
            return true
        }
        return false;
    }

    urlParamToJson(urlParams): any {
        const jsonString = '{"' + JSON.stringify(urlParams).replace(/"/g, '').replace(/=/g, '":"').replace(/&/g, '","') + '"}';

        console.log(jsonString);

        return JSON.parse(jsonString)
    }
}
