import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const apiUrl = environment.api_url;

@Injectable({
    providedIn: 'root'
})
export class TwitterApiService {

    constructor(
        private http: HttpClient
    ) {

    }

    getRequest(path, params): Observable<any> {
        return this.http.get(`${apiUrl}/twitter-api?path=${path}&params=${JSON.stringify(params)}`)
            .pipe(
                catchError(this.handleError('getRequest'))
            );
    }

    postRequest(path, params): Observable<any>  {
        return this.http.post(`${apiUrl}/twitter-api`, {path: path, params: params})
            .pipe(
                catchError(this.handleError('postRequest'))
            );
    }

    searchTweets(params): Observable<any> {
        return this.http.post(`${apiUrl}/twitter-api/search/tweets`, params)
            .pipe(
                catchError(this.handleError('searchTweets'))
            );
    }

    statusesOembed(url): Observable<any>  {
        return this.http.get(`${apiUrl}/twitter-api/oembed?url=${url}`)
            .pipe(
                catchError(this.handleError('statusesOembed'))
            );
    }

    oauthRequestToken(params): Observable<any>  {
        return this.http.post(`${apiUrl}/twitter-api/oauth/request-token`, params)
            .pipe(
                catchError(this.handleError('oauthRequestToken'))
            );
    }

    oauthAccessToken(params): Observable<any>  {
        return this.http.post(`${apiUrl}/twitter-api/oauth/access-token`, params)
            .pipe(
                catchError(this.handleError('oauthAccessToken'))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        }
    }
}
